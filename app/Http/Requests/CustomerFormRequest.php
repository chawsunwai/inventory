<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerFormRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        if ($this->method() == "PUT") {
            $imageRule = 'nullable';
        }else{
            $imageRule = 'required';
        }
        return [
            'name' => 'required|min:2',
            'email' => 'nullable',
            'phone' => 'required|min:8|unique:customers,phone,'.$this->customer,
            'address' => 'nullable',
            'image' => $imageRule,
        ];
    }

    public function messages() {
        return [
            'name.required' => 'The name field is required',
        ];
    }
}
