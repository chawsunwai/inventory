<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaleFormRequest;
use App\Models\Product;
use App\Models\Sale;
use App\Models\Customer;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class SaleController extends Controller {

    /**
     * @var Sale
     */
    private $sale;
    /**
     * @var Product
     */
    private $product;

    /**
     * SaleController constructor.
     * @param Sale $sale
     * @param Product $product
     */
    public function __construct(Sale $sale, Product $product) {
        $this->sale = $sale;
        $this->product = $product;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $sales = $this->sale->getSales(true);
        return view('sales.index', compact('sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $customers = Customer::get();
        $products = $this->product->getProducts();
        return view('sales.create', compact('customers', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaleFormRequest $request
     * @return void
     */
    public function store(SaleFormRequest $request) {
        $fields = $request->validated();
        $data = $this->sale->storeSale($fields);
        return redirect()->route('sales.index')->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id) {
        $data = $this->sale->getSaleById($id);
        return view('sales.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id) {
        $data = $this->sale->getSaleById($id, true);
        $data['customers'] = Customer::get();
        $data['products'] = $this->product->getProducts();
        return view('sales.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SaleFormRequest $request
     * @param int $id
     * @return Response
     */
    public function update(SaleFormRequest $request, $id) {
        $fields = $request->validated();
        $data = $this->sale->updateSale($fields, $id);
        return redirect()->route('sales.index')->with( $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id) {
        $data = $this->sale->destroySale($id);
        return json_encode($data);
    }

    /**
     * @param $id
     * @return false|string
     */
    public function deleteOrRestore($id){
        $data = $this->sale->deleteOrRestore($id);

        return json_encode($data);
    }

    /**
     * return product form to sales form
     * @param $index
     * @return Factory|View
     */
    public function productForm($index){
        $products = $this->product->getProducts();
        return view('sales.product-form', [
            'products' => $products,
            'index' => $index
        ]);
    }
}
