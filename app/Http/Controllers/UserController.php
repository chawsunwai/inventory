<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginFormRequest;
use App\Http\Requests\UserFormRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Date;
use Spatie\Permission\Models\Role;
use DB;


class UserController extends Controller
{
    private $user;

    /**
     * UserController constructor.
     * @param User $user
     */
    public function __construct(User $user){
        $this->user = $user;
    }

    /**
     * @param LoginFormRequest $request
     * @return RedirectResponse
     */
    public function doLogin(LoginFormRequest $request){
        $fields = $request->validated();
        $data = $this->user->doLogin($fields);

        if ($data['status']){
            return redirect()->route('dashboard');
        }

        return redirect()->back();
    }


    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }


    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        $users = User::paginate(100);

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::get();

        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserFormRequest $request)
    {
        $request->merge(['password' => Hash::make($request->get('password'))]);

        $request['created_by'] = auth()->id();
        $request['email_verified_at'] = Date::now();

        $user = User::create($request->all());

        $user->assignRole($user->role->name);

        return redirect()->route('users.index')->withStatus('User successfully created.');
    }


    /**
     * Display the specified user.
     *
     * @param string $name
     * @return Response
     */
    public function show(User $user) {
        return view('users.show', compact('user'));
    }



    /**
     * Show the form for editing the specified user
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserFormRequest $request, User $user)
    {
        $hasPassword = $request->get('password');

        $request->merge(['password' => Hash::make($request->get('password'))]);

        $request->except([$hasPassword ? '' : 'password']);

        $request['updated_by'] = auth()->id();

        $user->update($request->all());

        DB::table('model_has_roles')->where('model_id',$user->id)->delete();
    
        $user->assignRole($user->role->name);

        return redirect()->route('users.index')->withStatus('User successfully updated.');
    }

    /**
     * Remove the specified user from storage
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User  $user)
    {
        $user->delete();

        return redirect()->route('users.index')->withStatus('User successfully deleted.');
    }

    public function register()
    {
        $roles = Role::get();

        return view('user.register', compact('roles'));
    }

    public function doRegister(Request $request)
    {
        $request->merge(['password' => Hash::make($request->get('password'))]);

        $request['email_verified_at'] = Date::now();

        $user = User::create($request->all());

        $user->assignRole($user->role->name);

        return redirect()->route('user.login');
    }


}
