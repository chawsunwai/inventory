<?php

namespace App\Http\Controllers;

use App\Http\Requests\SupplierFormRequest;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SupplierController extends Controller {

    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * SupplierController constructor.
     * @param Supplier $supplier
     */
    public function __construct(Supplier $supplier) {
        $this->supplier = $supplier;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $suppliers = $this->supplier->getSuppliers(true);
        return view('suppliers.index', compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('suppliers.create');
    }

    /**
     * Store a newly created supplier in storage.
     *
     * @param SupplierFormRequest $request
     * @return Response
     */
    public function store(SupplierFormRequest $request) {
        $fields = $request->validated();
        $data = $this->supplier->storeSupplier($fields);
        return redirect()->route('suppliers.index')->with($data);
    }

    /**
     * Display the specified supplier.
     *
     * @param string $slug
     * @return Response
     */
    public function show($id) {
        $data = $this->supplier->getSupplierById($id);
        return view('suppliers.show', $data);
    }

    /**
     * Show the form for editing the specified supplier.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id) {
        $data = $this->supplier->getSupplierById($id);
        return view('suppliers.edit', $data);
    }

    /**
     * Update the specified supplier in storage.
     *
     * @param SupplierFormRequest $request
     * @param int $id
     * @return Response
     */
    public function update(SupplierFormRequest $request, $id) {
        $fields = $request->validated();
        $data = $this->supplier->updateSupplier($fields, $id);
        return redirect()->route('suppliers.index')->with($data);
    }

    /**
     * Remove the specified supplier from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id) {
        $data = $this->supplier->destroySupplier($id);
        return json_encode($data);
    }

    public function deleteOrRestore($id){
        $data = $this->supplier->deleteOrRestore($id);
        return json_encode($data);
    }
}
