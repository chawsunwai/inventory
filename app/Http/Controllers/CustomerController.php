<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerFormRequest;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CustomerController extends Controller {

    /**
     * @var Customer
     */
    private $customer;

    /**
     * CustomerController constructor.
     * @param Customer $customer
     */
    public function __construct(Customer $customer) {
        $this->customer = $customer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $customers = $this->customer->getCustomers(true);
        return view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('customers.create');
    }

    /**
     * Store a newly created customer in storage.
     *
     * @param CustomerFormRequest $request
     * @return Response
     */
    public function store(CustomerFormRequest $request) {
        $fields = $request->validated();
        $data = $this->customer->storeCustomer($fields);
        return redirect()->route('customers.index')->with($data);
    }

    /**
     * Display the specified customer.
     *
     * @param string $slug
     * @return Response
     */
    public function show($id) {
        $data = $this->customer->getCustomerById($id);
        return view('customers.show', $data);
    }

    /**
     * Show the form for editing the specified customer.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id) {
        $data = $this->customer->getCustomerById($id);
        return view('customers.edit', $data);
    }

    /**
     * Update the specified customer in storage.
     *
     * @param CustomerFormRequest $request
     * @param int $id
     * @return Response
     */
    public function update(CustomerFormRequest $request, $id) {
        $fields = $request->validated();
        $data = $this->customer->updateCustomer($fields, $id);
        return redirect()->route('customers.index')->with($data);
    }

    /**
     * Remove the specified customer from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id) {
        $data = $this->customer->destroyCustomer($id);
        return json_encode($data);
    }

    public function deleteOrRestore($id){
        $data = $this->customer->deleteOrRestore($id);

        return json_encode($data);
    }
}
