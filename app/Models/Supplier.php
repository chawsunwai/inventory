<?php

namespace App\Models;

use App\Utils\AppUtils;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Supplier extends Model{

    use SoftDeletes;

    protected $fillable = [
        'name', 'email', 'phone', 'image', 'address', 'created_by', 'updated_by'
    ];

    // ===================== ORM Definition START ===================== //

    /**
     * @return HasMany
     */
    public function products(){
        return $this->hasMany(Product::class);
    }

    /**
     * @return BelongsToMany
     */
    public function purchases(){
        return $this->hasMany(Purchase::class);
    }

    /**
     * @return BelongsTo
     */
    public function createdBy(){
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return BelongsTo
     */
    public function updatedBy(){
        return $this->belongsTo(User::class, 'updated_by');
    }

    // ===================== ORM Definition END ===================== //

    /**
     * get specified supplier by id
     * @param int $id
     * @param bool $withTrashed
     * @return array
     */
    public function getSupplierById(int $id, $withTrashed = false) {

        $supplier = $this->newQuery()
            ->where('id', $id)
            ->when($withTrashed, function ($query){
                $query->withTrashed();
            })
            ->firstOrFail()->format();

        $data = [];
        if ($supplier){
            $data['status'] = true;
            $data['supplier'] = $supplier;
        }else{
            $data['status'] = false;
            $data['supplier'] = [];
        }

        return $data;
    }

    /**
     * @param string $name
     * @param bool $withTrashed
     * @return mixed
     */
    public function getSupplierByName(string $name, $withTrashed = false) {
        $supplier = $this::where('name', $name)
            ->when($withTrashed, function ($query){
                $query->withTrashed();
            })
            ->with([
                'purchases'
            ])
            ->firstOrFail()->format();

        $data['status'] = true;
        $data['supplier'] = $supplier;

        return $data;
    }
    /**
     * @param bool $withTrashed
     * @return Collection
     */
    public function getSuppliers($withTrashed = false) {
        return $this->when($withTrashed, function ($query){
                $query->withTrashed();
            })
            ->get()->map(function ($supplier){
                return $supplier->format();
            });
    }
    /**
     * format supplier object
     * @return array
     */
    public function format(){
        return [
            'id' => $this->id,
            'created_by' => $this->createdBy->name,
            'updated_by' => $this->updateddBy ? $this->updateddBy->name : '-',
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'image' => asset('storage/'.$this->image),
            'address' => $this->address,
            'created_at' => $this->created_at,
            'updated_at' => $this->created_at,
            'deleted_at' => $this->deleted_at,
        ];
    }

    /**
     * store new supplier
     * @param array $fields
     * @return array
     */
    public function storeSupplier(array $fields) {
        $supplierImage = $fields['image'];
        $supplierImage = AppUtils::base64ToImage($supplierImage);
        $imageName = time() . 'supplier.png';
        $fields['created_by'] = auth()->id();
        $fields['image'] = 'supplier/' . $imageName;
        $supplier = $this::create($fields);

        $data = [];
        if ($supplier) {
            $data['status'] = true;
            $data['message'] = "New Supplier was added";
            Storage::disk()->put('supplier/' . $imageName, $supplierImage);
        } else {
            $data['status'] = false;
            $data['message'] = "There is problem adding new supplier";
        }

        return $data;
    }

    /**
     * @param array $fields
     * @param int $id
     * @return mixed
     */
    public function updateSupplier(array $fields, int $id) {
        $supplier = $this::withTrashed()->findOrFail($id);
        if ($supplier){
            if ($fields['image']){
                $oldImage = $supplier->image;
                $supplierImage = $fields['image'];
                $supplierImage = AppUtils::base64ToImage($supplierImage);
                $imageName = time() . 'supplier.png';
                $fields['image'] = 'supplier/' . $imageName;
                Storage::disk()->put('supplier/' . $imageName, $supplierImage);
                unlink(storage_path('app/public/' . $oldImage));
            }else{
                unset($fields['image']);
            }

            $fields['updated_by'] = auth()->id();
            $supplier->update($fields);
            $data['status'] = true;
            $data['message'] = "Supplier was updated";
        }else{
            $data['status'] = false;
            $data['message'] = "There is problem updating supplier";
        }

        return $data;
    }

    /**
     * @param int $id
     * @return array
     */
    public function destroySupplier(int $id) {
        $supplier = $this::withTrashed()->where('id', $id)->firstOrFail();
        $data = [];
        if ($supplier){
            $oldImage = $supplier->image;
            $supplier->forceDelete();
            unlink(storage_path('app/public/' . $oldImage));
            $data['status'] = true;
            $data['message'] = "Supplier was deleted";
        }else{
            $data['status'] = false;
            $data['message'] = "There is problem deleting supplier";
        }

        return $data;
    }

    /**
     * @param $id
     * @return array
     */
    public function deleteOrRestore($id) {
        $data = [];
        $supplier = $this::withTrashed()->findOrFail($id);
        if ($supplier){
            if ($supplier->trashed()){
                $supplier->restore();
            }else{
                try {
                    $supplier->delete();
                } catch (Exception $e) {
                }
            }
            $data['status'] = true;
            $data['message'] = "Supplier status was updated";
        }else {
            $data['status'] = false;
            $data['message'] = "Supplier not found";
        }

        return $data;
    }

}
