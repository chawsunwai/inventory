<?php

namespace App\Models;

use App\Utils\AppUtils;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Customer extends Model{

    use SoftDeletes;

    protected $fillable = [
        'name', 'email', 'phone', 'address', 'image', 'created_by', 'updated_by'
    ];

    // ===================== ORM Definition START ===================== //

    /**
     * @return HasMany
     */
    public function orders(){
        return $this->hasMany(Order::class);
    }

    /**
     * @return BelongsTo
     */
    public function createdBy(){
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return BelongsTo
     */
    public function updatedBy(){
        return $this->belongsTo(User::class, 'updated_by');
    }

    // ===================== ORM Definition END ===================== //

    /**
     * get specified customer by id
     * @param int $id
     * @param bool $withTrashed
     * @return array
     */
    public function getCustomerById(int $id, $withTrashed = false) {

        $customer = $this->newQuery()
            ->where('id', $id)
            ->when($withTrashed, function ($query){
                $query->withTrashed();
            })
            ->firstOrFail()->format();

        $data = [];
        if ($customer){
            $data['status'] = true;
            $data['customer'] = $customer;
        }else{
            $data['status'] = false;
            $data['customer'] = [];
        }

        return $data;
    }

    /**
     * @param string $name
     * @param bool $withTrashed
     * @return mixed
     */
    public function getCustomerByName(string $name, $withTrashed = false) {
        $customer = $this::where('name', $name)
            ->when($withTrashed, function ($query){
                $query->withTrashed();
            })
            ->with([
                'purchases'
            ])
            ->firstOrFail()->format();

        $data['status'] = true;
        $data['customer'] = $customer;

        return $data;
    }
    /**
     * @param bool $withTrashed
     * @return Collection
     */
    public function getCustomers($withTrashed = false) {
        return $this->when($withTrashed, function ($query){
                $query->withTrashed();
            })
            ->get()->map(function ($customer){
                return $customer->format();
            });
    }
    /**
     * format customer object
     * @return array
     */
    public function format(){
        return [
            'id' => $this->id,
            'created_by' => $this->createdBy->name,
            'updated_by' => $this->updateddBy ? $this->updateddBy->name : '-',
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'image' => asset('storage/'.$this->image),
            'address' => $this->address,
            'created_at' => $this->created_at,
            'updated_at' => $this->created_at,
            'deleted_at' => $this->deleted_at,
        ];
    }

    /**
     * store new customer
     * @param array $fields
     * @return array
     */
    public function storeCustomer(array $fields) {
        $customerImage = $fields['image'];
        $customerImage = AppUtils::base64ToImage($customerImage);
        $imageName = time() . 'customer.png';
        $fields['created_by'] = auth()->id();
        $fields['image'] = 'customer/' . $imageName;
        $customer = $this::create($fields);

        $data = [];
        if ($customer) {
            $data['status'] = true;
            $data['message'] = "New Customer was added";
            Storage::disk()->put('customer/' . $imageName, $customerImage);
        } else {
            $data['status'] = false;
            $data['message'] = "There is problem adding new customer";
        }

        return $data;
    }

    /**
     * @param array $fields
     * @param int $id
     * @return mixed
     */
    public function updateCustomer(array $fields, int $id) {
        $customer = $this::withTrashed()->findOrFail($id);
        if ($customer){
            if ($fields['image']){
                $oldImage = $customer->image;
                $customerImage = $fields['image'];
                $customerImage = AppUtils::base64ToImage($customerImage);
                $imageName = time() . 'customer.png';
                $fields['image'] = 'customer/' . $imageName;
                Storage::disk()->put('customer/' . $imageName, $customerImage);
                unlink(storage_path('app/public/' . $oldImage));
            }else{
                unset($fields['image']);
            }

            $fields['updated_by'] = auth()->id();
            $customer->update($fields);
            $data['status'] = true;
            $data['message'] = "Customer was updated";
        }else{
            $data['status'] = false;
            $data['message'] = "There is problem updating customer";
        }

        return $data;
    }

    /**
     * @param int $id
     * @return array
     */
    public function destroyCustomer(int $id) {
        $customer = $this::withTrashed()->where('id', $id)->firstOrFail();
        $data = [];
        if ($customer){
            $oldImage = $customer->image;
            $customer->forceDelete();
            unlink(storage_path('app/public/' . $oldImage));
            $data['status'] = true;
            $data['message'] = "Customer was deleted";
        }else{
            $data['status'] = false;
            $data['message'] = "There is problem deleting customer";
        }

        return $data;
    }

    /**
     * @param $id
     * @return array
     */
    public function deleteOrRestore($id) {
        $data = [];
        $customer = $this::withTrashed()->findOrFail($id);
        if ($customer){
            if ($customer->trashed()){
                $customer->restore();
            }else{
                try {
                    $customer->delete();
                } catch (Exception $e) {
                }
            }
            $data['status'] = true;
            $data['message'] = "Customer status was updated";
        }else {
            $data['status'] = false;
            $data['message'] = "Customer not found";
        }

        return $data;
    }

}

