$('#supplierForm').validate({
    rules: rules,
    errorClass: "help-block error",
    highlight: function (e) {
        $(e).closest(".form-group.row").addClass("has-error")
    },
    unhighlight: function (e) {
        $(e).closest(".form-group.row").removeClass("has-error")
    },
    errorPlacement: function(error, element) {
        if (element.hasClass('select2_select')) {
            error.insertAfter(element.siblings('span'));
        } else {
            error.insertAfter(element);
        }
    }
});

//
let imagePreview = $('#supplierImagePreview');
let imagePicker = $('#supplier_image_picker');
let imageField = $('#image');

initImageCropperModal(imagePreview, imagePicker, imageField);

$(document).on('change', '#supplier_image_picker', function (e) {
    let imagePath = $(this).val();
    let allowedExtensions = /(\.jpg|\.png|\.jpeg)$/i;

    if (!allowedExtensions.exec(imagePath)) {
        alert('Image format is not correct');
        $(this).val('');
        $(imagePreview).attr('src', defaultImage);
        return false;
    } else {
        showImageCropper(e);
    }
});