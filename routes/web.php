<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['user']], function () {
    Route::get('/', 'HomeController@index')->name('dashboard');

    // categories
    Route::resource('categories', 'CategoryController');
    Route::prefix('categories')->group(function (){
        Route::get('/delete-or-restore/{category}', 'CategoryController@deleteOrRestore')->name('categories.delete');
    });

    // products
    Route::resource('products', 'ProductController');
    Route::prefix('products')->group(function (){
        Route::get('/delete-or-restore/{product}', 'ProductController@deleteOrRestore')->name('products.delete');
    });

    // purchases
    Route::resource('purchases', 'PurchaseController');
    Route::prefix('purchases')->group(function (){
        Route::get('/delete-or-restore/{purchase}', 'PurchaseController@deleteOrRestore')->name('purchases.delete');
        Route::get('/product-form/{index?}', 'PurchaseController@productForm')->name('purchases.productForm');
    });

    // sales
    Route::resource('sales', 'SaleController');
    Route::prefix('sales')->group(function (){
        Route::get('/delete-or-restore/{sale}', 'SaleController@deleteOrRestore')->name('sales.delete');
        Route::get('/product-form/{index?}', 'SaleController@productForm')->name('sales.productForm');
    });

    // suppliers
    Route::resource('suppliers', 'SupplierController');
    Route::prefix('suppliers')->group(function (){
        Route::get('/delete-or-restore/{supplier}', 'SupplierController@deleteOrRestore')->name('suppliers.delete');
    });

    // customers
    Route::resource('customers', 'CustomerController');
    Route::prefix('customers')->group(function (){
        Route::get('/delete-or-restore/{customer}', 'CustomerController@deleteOrRestore')->name('customers.delete');
    });

    // users
    Route::resource('users', 'UserController');
    Route::prefix('users')->group(function (){
        Route::get('/delete-or-restore/{user}', 'UserController@deleteOrRestore')->name('users.delete');
    });

    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::match(['put', 'patch'], 'profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::match(['put', 'patch'], 'profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

    Route::resource('roles', 'RoleController');
    Route::prefix('roles')->group(function (){
    Route::get('/delete-or-restore/{role}', 'RoleController@deleteOrRestore')->name('roles.delete');
    });

    Route::resource('permissions', 'PermissionController');
    Route::prefix('permissions')->group(function (){
    Route::get('/delete-or-restore/{permission}', 'PermissionController@deleteOrRestore')->name('permissions.delete');
    });

});


Route::view('user/login', 'user.login')->name('user.login');
Route::post('user/do-login', 'UserController@doLogin')->name('user.doLogin');
Route::get('user/register', 'UserController@register')->name('user.register');
Route::post('user/do-register', 'UserController@doRegister')->name('user.doRegister');
Route::post('/logout', 'UserController@logout')->name('logout');