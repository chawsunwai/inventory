<footer class="page-footer">
    <div class="font-13">{{now()->year}} © <b>Admin</b> - All rights reserved.</div>
    <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
</footer>