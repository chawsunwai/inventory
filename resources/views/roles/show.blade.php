@extends('layouts.master')

@section('title', $role['name'])

@section('content')

    @include('partials._crop-image-modal')

    <div class="page-heading">
        <h1 class="page-title">Role Detail</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}"><i class="fa fa-home font-20"></i></a>
            </li>
            <li class="breadcrumb-item">Role Detail</li>
        </ol>
    </div>

    <div class="page-content fade-in-up">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Role</div>
            </div>
            <div class="ibox-body">
                <div class="row">
                    <div class="col-md-12 row mb-3">
                        <div class="col-3 font-weight-bold">Role Name:</div>
                        <div class="col-md-9">{{ $role->name }}</div>
                    </div>
                    <div class="col-md-12 row mb-3">
                        <div class="col-3 font-weight-bold">Permissions:</div>
                        <div class="col-md-9">
                            @if(!empty($rolePermissions))
                                @foreach($rolePermissions as $v)
                                    <label class="label label-success">{{ $v->name }}</label>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection