<div class="page-content">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">{{isset($role) ? 'Edit' : 'Create'}} Role</div>
        </div>
        <div class="ibox-body">
            <div class="row">
                <div class="col-md-8">
                    <form action="{{isset($role) ? route('roles.update', $role['id']) : route('roles.store')}}" method="post" novalidate="novalidate" id="roleForm" enctype="multipart/form-data">
                        @csrf
                        @if(isset($role))
                            @method('PUT')
                        @endif
                        <div class="form-group required row">
                            <label for="role_name" class="col-sm-3 col-form-label">Role Name:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'role_name')}}" name="name" value="{{isset($role) ? $role['name'] : old('name')}}" id="name" type="text" placeholder="Role Name">
                                @include('partials._error', ['field' => 'name'])
                            </div>
                        </div>
                        <div class="form-group required row">
                            <label for="permission_id" class="col-sm-3 col-form-label">Permission:</label>
                            <div class="col-sm-9">
                                @if(isset($role))
                                    @foreach($permission as $value)
                                        <span style="display: inline-block;margin-bottom: .5rem;">
                                            <input type="checkbox" name="permission[]" id="permission[]" value="{{ $value->id }}" {{ in_array($value->id, $rolePermissions) ? 'checked' : ''}}>  {{ $value->name }}
                                        </span>
                                        <br/>
                                    @endforeach
                                @else
                                    @foreach($permission as $value)
                                        <span style="display: inline-block;margin-bottom: .5rem;">
                                            <input type="checkbox" name="permission[]" id="permission[]" value="{{ $value->id }}" {{ $value->id == null ? 'checked' : ''}}>  {{ $value->name }}
                                        </span>
                                        <br/>
                                    @endforeach
                                @endif
                                @include('partials._error', ['field' => 'permission_id'])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary btn-block">{{isset($role) ? 'Update' : 'Create'}} Role</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>