@extends('layouts.master')

@section('title', 'Edit Role')

@section('content')

    @include('partials._crop-image-modal')

    <div class="page-heading">
        <h1 class="page-title">Edit Role</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}"><i class="fa fa-home font-20"></i></a>
            </li>
            <li class="breadcrumb-item">Edit Role</li>
        </ol>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif

    @include('roles.form')

@endsection