<div class="page-content">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">{{isset($customer) ? 'Edit' : 'Create'}} Customer</div>
        </div>
        <div class="ibox-body">
            <div class="row">
                <div class="col-md-8">
                    <form action="{{isset($customer) ? route('customers.update', $customer['id']) : route('customers.store')}}" method="post" novalidate="novalidate" id="customerForm" enctype="multipart/form-data">
                        @csrf
                        @if(isset($customer))
                            @method('PUT')
                        @endif
                        <div class="form-group required row">
                            <label for="customer_name" class="col-sm-3 col-form-label">Customer Name:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'customer_name')}}" name="name" value="{{isset($customer) ? $customer['name'] : old('name')}}" id="name" type="text" placeholder="Customer Name">
                                @include('partials._error', ['field' => 'name'])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="customer_email" class="col-sm-3 col-form-label">Customer Email:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'email')}}" name="email" value="{{isset($customer) ? $customer['email'] : old('email')}}" id="email" type="text" placeholder="Customer Email">
                                @include('partials._error', ['field' => 'email'])
                            </div>
                        </div>
                        <div class="form-group required row">
                            <label for="customer_phone" class="col-sm-3 col-form-label">Customer Phone:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'phone')}}" name="phone" value="{{isset($customer) ? $customer['phone'] : old('phone')}}" id="phone" type="number" placeholder="Customer Phone">
                                @include('partials._error', ['field' => 'phone'])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="customer_address" class="col-sm-3 col-form-label">Customer Address:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'address')}}" name="address" value="{{isset($customer) ? $customer['address'] : old('address')}}" id="address" type="text" placeholder="Customer Address">
                                @include('partials._error', ['field' => 'address'])
                            </div>
                        </div>
                        <div class="form-group {{isset($customer) ? '' : 'required'}} row">
                            <label for="customer_image_picker" class="col-sm-3 col-form-label">Customer Image:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'image')}}"  id="customer_image_picker" type="file" accept=".png, .jpg">
                                <input type="text" style="position: absolute; z-index: -1" name="image" value="" id="image">
                                @include('partials._error', ['field' => 'image'])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary btn-block">{{isset($customer) ? 'Update' : 'Create'}} Customer</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <img src="{{isset($customer) ? $customer['image'] : asset('assets/img/image.png')}}" class="img-fluid" id="customerImagePreview" alt="customer">
                </div>
            </div>
        </div>
    </div>
</div>