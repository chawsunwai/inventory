@extends('layouts.master')

@section('title', $customer['name'])

@section('css')
    <link href="{{asset('assets/css/datatables.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
    <div class="page-heading">
        <h1 class="page-title">Customer Detail</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}"><i class="fa fa-home font-20"></i></a>
            </li>
            <li class="breadcrumb-item">Customer Detail</li>
        </ol>
    </div>

    <div class="page-content fade-in-up">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Customer</div>
            </div>
            <div class="ibox-body">
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{$customer['image']}}" class="img-fluid" alt="customer">
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Customer Name:</div>
                                <div class="col-md-7">{{$customer['name']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Customer Email:</div>
                                <div class="col-md-7">{{$customer['email']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Customer Phone:</div>
                                <div class="col-md-7">{{$customer['phone']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Customer Address:</div>
                                <div class="col-md-7">{{$customer['address']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Created By:</div>
                                <div class="col-md-7">{{$customer['created_by']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Updated By:</div>
                                <div class="col-md-7">{{$customer['updated_by']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Created At:</div>
                                <div class="col-md-7">{{$customer['created_at']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Updated At:</div>
                                <div class="col-md-7">{{$customer['updated_at']}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('assets/js/datatables.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        let dataTable = $('#purchases-table').DataTable({
            pageLength: 10,
            responsive: true,
        });
    </script>
@endsection