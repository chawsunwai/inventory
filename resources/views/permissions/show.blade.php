@extends('layouts.master')

@section('title', $permission['name'])

@section('content')

    @include('partials._crop-image-modal')

    <div class="page-heading">
        <h1 class="page-title">Permission Detail</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}"><i class="fa fa-home font-20"></i></a>
            </li>
            <li class="breadcrumb-item">Permission Detail</li>
        </ol>
    </div>

    <div class="page-content fade-in-up">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Permission</div>
            </div>
            <div class="ibox-body">
                <div class="row">
                    <div class="col-md-12 row mb-3">
                        <div class="col-3 font-weight-bold">Permission Name:</div>
                        <div class="col-md-9">{{ $permission->name }}</div>
                    </div>
                    <div class="col-md-12 row mb-3">
                        <div class="col-3 font-weight-bold">Guard Name:</div>
                        <div class="col-md-9">{{ $permission->guard_name }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection