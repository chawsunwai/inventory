<div class="page-content">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">{{isset($permission) ? 'Edit' : 'Create'}} Permission</div>
        </div>
        <div class="ibox-body">
            <div class="row">
                <div class="col-md-8">
                    <form action="{{isset($permission) ? route('permissions.update', $permission['id']) : route('permissions.store')}}" method="post" novalidate="novalidate" id="permissionForm" enctype="multipart/form-data">
                        @csrf
                        @if(isset($permission))
                            @method('PUT')
                        @endif
                        <div class="form-group required row">
                            <label for="permission_name" class="col-sm-3 col-form-label">Permission Name:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'permission_name')}}" name="name" value="{{isset($permission) ? $permission['name'] : old('name')}}" id="name" type="text" placeholder="Permission Name">
                                @include('partials._error', ['field' => 'name'])
                            </div>
                        </div>
                        <div class="form-group required row">
                            <label for="guard_name" class="col-sm-3 col-form-label">Guard Name:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'guard_name')}}" name="guard_name" value="{{isset($permission) ? $permission['guard_name'] : old('guard_name')}}" id="guard_name" type="text" placeholder="Guard Name">
                                @include('partials._error', ['field' => 'guard_name'])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary btn-block">{{isset($permission) ? 'Update' : 'Create'}} Permission</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>