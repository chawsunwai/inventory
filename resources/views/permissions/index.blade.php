@extends('layouts.master')

@section('title', 'Manage Permissions')

@section('css')
    <link href="{{asset('assets/css/datatables.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
    <div class="page-heading">
        <h1 class="page-title">Manage Permissions</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}"><i class="fa fa-home font-20"></i></a>
            </li>
            <li class="breadcrumb-item">Manage Permissions</li>
        </ol>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="page-content fade-in-up">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Permissions</div>
                <div class="col-4">
                @can('permission-create')
                    <a class="btn btn-success" href="{{ route('permissions.create') }}"> Create Permission</a>
                @endcan
                </div>
            </div>
            <div class="ibox-body">
                <table class="table table-striped table-bordered table-hover" id="permission-table" cellspacing="0" width="100%">
                    <thead class=" text-primary">
                        <th scope="col">{{ __('No') }}</th>
                        <th scope="col">{{ __('Name') }}</th>
                        <th scope="col"></th>
                    </thead>
                    <tbody>
                        @foreach ($permissions as $permission)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $permission->name }}</td>
                                <td class="text-center">
                                    <a href="{{route('permissions.show', $permission['id'])}}" class="btn btn-info btn-xs m-r-5" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye font-14"></i></a>
                                    <a href="{{route('permissions.edit', $permission['id'])}}" class="btn btn-primary btn-xs m-r-5" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil font-14"></i></a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{asset('assets/js/datatables.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        let dataTable = $('#permission-table').DataTable({
            pageLength: 10,
            responsive: true
        });

        $(document).on('change', '.soft-delete-permission', function (e) {
            let url = $(this).attr('data-url');

            $.get(url, function (response) {
                if (JSON.parse(response).status){
                    showToast('Success', JSON.parse(response).message, 'success');
                }else {
                    showToast('Error', JSON.parse(response).message, 'error');
                }
            })
        });

        $(document).on('click', '.delete-permission', function (e) {
            e.preventDefault();
            let isDelete = confirm('Do you really want to permanently delete?');
            if (isDelete){
                let row = $(this).parents('tr');
                let url = $(this).attr('href');
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    success: function (response) {
                        if (JSON.parse(response).status){
                            showToast('Success', JSON.parse(response).message, 'success');
                            dataTable.row(row).remove().draw(false);
                        } else {
                            showToast('Error', JSON.parse(response).message, 'error');
                        }
                    }
                });
            }
        });
    </script>
@endsection