@extends('layouts.master')

@section('title', 'Create User')

@section('content')

@include('partials._crop-image-modal')

    <div class="page-heading">
        <h1 class="page-title">Create User</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}"><i class="fa fa-home font-20"></i></a>
            </li>
            <li class="breadcrumb-item">Create User</li>
        </ol>
    </div>

    <div class="page-content">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Create User</div>
            </div>
            <div class="ibox-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" action="{{ route('users.store') }}" autocomplete="off">
                            @csrf
                            <div class="form-group required {{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Name') }}" value="{{ old('name') }}" required autofocus>
                                @include('alerts.feedback', ['field' => 'name'])
                            </div>
                            <div class="form-group required {{ $errors->has('email') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                <input type="email" name="email" id="input-email" class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" value="{{ old('email') }}" required>
                                @include('alerts.feedback', ['field' => 'email'])
                            </div>
                            <div class="form-group required {{ $errors->has('password') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-password">{{ __('Password') }}</label>
                                <input type="password" name="password" id="input-password" class="form-control form-control-alternative{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ __('Password') }}" value="" required>
                                @include('alerts.feedback', ['field' => 'password'])
                            </div>
                            <div class="form-group required">
                                <label class="form-control-label" for="input-password-confirmation">{{ __('Confirm Password') }}</label>
                                <input type="password" name="password_confirmation" id="input-password-confirmation" class="form-control form-control-alternative" placeholder="{{ __('Confirm Password') }}" value="" required>
                            </div>
                            <div class="form-group required">
                                <label for="role_id" class="orm-control-label">Role:</label>
                                <select class="form-control select2_select select2-hidden-accessible" name="role_id" id="role_id">
                                    <option></option>
                                    @foreach($roles as $role)
                                        <option value="{{$role['id']}}" {{ (old('role_id') == $role['id'] ? 'selected' : '')}}>{{$role['name']}}</option>
                                    @endforeach
                                </select>
                                @include('alerts.feedback', ['field' => 'role_id'])
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
