@extends('layouts.master')

@section('title', $user['name'])

@section('content')
    <div class="page-heading">
        <h1 class="page-title">User Detail</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}"><i class="fa fa-home font-20"></i></a>
            </li>
            <li class="breadcrumb-item">User Detail</li>
        </ol>
    </div>

    <div class="page-content fade-in-up">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">User</div>
            </div>
            <div class="ibox-body">
                <div class="row">
                    <div class="col-md-12 row mb-3">
                        <div class="col-5 font-weight-bold">User Name:</div>
                        <div class="col-md-7">{{$user['name']}}</div>
                    </div>
                    <div class="col-md-12 row mb-3">
                        <div class="col-5 font-weight-bold">User Email:</div>
                        <div class="col-md-7">{{$user['email']}}</div>
                    </div>
                    <div class="col-md-12 row mb-3">
                        <div class="col-5 font-weight-bold">User Role:</div>
                        <div class="col-md-7">{{$user->role->name}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection