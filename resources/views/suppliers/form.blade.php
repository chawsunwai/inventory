<div class="page-content">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">{{isset($supplier) ? 'Edit' : 'Create'}} Supplier</div>
        </div>
        <div class="ibox-body">
            <div class="row">
                <div class="col-md-8">
                    <form action="{{isset($supplier) ? route('suppliers.update', $supplier['id']) : route('suppliers.store')}}" method="post" novalidate="novalidate" id="supplierForm" enctype="multipart/form-data">
                        @csrf
                        @if(isset($supplier))
                            @method('PUT')
                        @endif
                        <div class="form-group required row">
                            <label for="supplier_name" class="col-sm-3 col-form-label">Supplier Name:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'supplier_name')}}" name="name" value="{{isset($supplier) ? $supplier['name'] : old('name')}}" id="name" type="text" placeholder="Supplier Name">
                                @include('partials._error', ['field' => 'name'])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="supplier_email" class="col-sm-3 col-form-label">Supplier Email:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'email')}}" name="email" value="{{isset($supplier) ? $supplier['email'] : old('email')}}" id="email" type="text" placeholder="Supplier Email">
                                @include('partials._error', ['field' => 'email'])
                            </div>
                        </div>
                        <div class="form-group required row">
                            <label for="supplier_phone" class="col-sm-3 col-form-label">Supplier Phone:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'phone')}}" name="phone" value="{{isset($supplier) ? $supplier['phone'] : old('phone')}}" id="phone" type="number" placeholder="Supplier Phone">
                                @include('partials._error', ['field' => 'phone'])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="supplier_address" class="col-sm-3 col-form-label">Supplier Address:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'address')}}" name="address" value="{{isset($supplier) ? $supplier['address'] : old('address')}}" id="address" type="text" placeholder="Supplier Address">
                                @include('partials._error', ['field' => 'address'])
                            </div>
                        </div>
                        <div class="form-group {{isset($supplier) ? '' : 'required'}} row">
                            <label for="supplier_image_picker" class="col-sm-3 col-form-label">Supplier Image:</label>
                            <div class="col-sm-9">
                                <input class="form-control {{\App\Utils\AppUtils::inputFieldError($errors, 'image')}}"  id="supplier_image_picker" type="file" accept=".png, .jpg">
                                <input type="text" style="position: absolute; z-index: -1" name="image" value="" id="image">
                                @include('partials._error', ['field' => 'image'])
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary btn-block">{{isset($supplier) ? 'Update' : 'Create'}} Supplier</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <img src="{{isset($supplier) ? $supplier['image'] : asset('assets/img/image.png')}}" class="img-fluid" id="supplierImagePreview" alt="supplier">
                </div>
            </div>
        </div>
    </div>
</div>