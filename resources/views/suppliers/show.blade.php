@extends('layouts.master')

@section('title', $supplier['name'])

@section('content')
    <div class="page-heading">
        <h1 class="page-title">Supplier Detail</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}"><i class="fa fa-home font-20"></i></a>
            </li>
            <li class="breadcrumb-item">Supplier Detail</li>
        </ol>
    </div>

    <div class="page-content fade-in-up">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Supplier</div>
            </div>
            <div class="ibox-body">
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{$supplier['image']}}" class="img-fluid" alt="supplier">
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Supplier Name:</div>
                                <div class="col-md-7">{{$supplier['name']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Supplier Email:</div>
                                <div class="col-md-7">{{$supplier['email']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Supplier Phone:</div>
                                <div class="col-md-7">{{$supplier['phone']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Supplier Address:</div>
                                <div class="col-md-7">{{$supplier['address']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Created By:</div>
                                <div class="col-md-7">{{$supplier['created_by']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Updated By:</div>
                                <div class="col-md-7">{{$supplier['updated_by']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Created At:</div>
                                <div class="col-md-7">{{$supplier['created_at']}}</div>
                            </div>
                            <div class="col-md-6 row mb-3">
                                <div class="col-5 font-weight-bold">Updated At:</div>
                                <div class="col-md-7">{{$supplier['updated_at']}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection